# README #

API de Clientes para o teste prático do processo seletivo (Pergunta 02).

### O que a API faz? ###

É uma API para gerenciamento de Clientes, com todas as funções de CRUD. Ela recebe e responde apenas requisições JSON.
As informações são persistidas em uma base de dados MongoDB.
Ela depende da API de Campanhas, mas consegue continuar atendendo as requisições de leitura e cadastro mesmo se a API de Campanhas ficar temporariamente indisponível.

O teste de carga da criação de usuários demonstrou que a aplicação suporta com facilidade 300 request por segundo, com média de tempo de resposta de 376ms.

O teste de carga de consulta de campanhas disponíveis, que faz a API de clientes consultar a API de campanhas, atingiu 310 requisições por segundo, com média de tempo de resposta de 417ms.

### Como testar? ###

O projeto inclui um arquivo do Postman, para facilitar os testes manuais da API.

Também há dois arquivos do JMeter com os testes que foram realizados de criação de usuários e consulta de campanhas, juntamente com arquivos de massa de testes. (na pasta raiz)

Existe uma versão hospedada no Heroku, na seguinte URL:

https://murmuring-anchorage-38914.herokuapp.com/

** O primeiro request pode demorar, pois a máquina é desativada depois de 30 minutos de inatividade **

Para rodar a API numa máquina local, é necessário editar o application.properties para o perfil "local".
A aplicação vai apontar para uma instância local padrão do MongoDB.

Também é necessário que a API de campanhas esteja rodando no Start up da API de clientes. Depois disso a API de clientes consegue responder leituras e escritas sem problemas, mas as alterações só persistidas escritas quando a API de Campanhas voltar a ficar disponível. Somente as informações relativas aos clientes é que são persistidas sem necessidade da API de Campanhas.

As dependências do projeto são gerenciadas via Maven.
O projeto foi iniciado com um esqueleto do Spring Boot (http://start.spring.io/), com dependências Web, Cache e MongoDB.

### Testes unitários ###

Foram desenvolvidos testes unitários na camada de negócios da aplicação. Tenho executado eles com o JUnit para garantir a integridade das regras. (Com Mocks para a base/repositórios)