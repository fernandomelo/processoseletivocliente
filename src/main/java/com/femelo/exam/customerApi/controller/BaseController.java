package com.femelo.exam.customerApi.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.femelo.exam.customerApi.model.BaseModel;

@RestController
public interface BaseController<T extends BaseModel> {
	
	public abstract ResponseEntity<T> create(T entity, Pageable pageable);
	
	public abstract ResponseEntity<T> update(String id, T entity);
	
	public abstract ResponseEntity<T> delete(String id);
	
	public abstract ResponseEntity<T> get(String id);
	
}