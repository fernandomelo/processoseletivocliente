package com.femelo.exam.customerApi.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.femelo.exam.customerApi.business.CampaignBusiness;
import com.femelo.exam.customerApi.business.CustomerBusiness;
import com.femelo.exam.customerApi.helper.CustomerValidationException;
import com.femelo.exam.customerApi.helper.JsonMessage;
import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.model.Campaign;
import com.femelo.exam.customerApi.model.Customer;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController implements BaseController<Customer> {
	
	@Autowired
	private CampaignBusiness campaignBusiness;
	
	@Autowired
	private CustomerBusiness customerBusiness;
	
	public CustomerController(CustomerBusiness customerBusiness, CampaignBusiness campaignBusiness) {
		this.customerBusiness = customerBusiness;
		this.campaignBusiness = campaignBusiness;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity create(@RequestBody Customer entity, Pageable pageable) {
		Customer customer = customerBusiness.findByEmail(entity.getEmail());
		
		if (Objects.nonNull(customer)) {
			if (customer.getCampaigns().size() > 0) {
				return conflict();
			}
			else {
				return getAvailableCampaigns(customer.getTeamId(), pageable);
			}
		}
		
		Customer created = null;
		try {
			created = customerBusiness.create(entity);
		} catch (CustomerValidationException e) {
			return badRequest();
		}
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Location", "/customer/" + created.getId());
		
		return new ResponseEntity<Customer>(created, headers, HttpStatus.CREATED);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Customer> update(@PathVariable("id") String id, @RequestBody Customer entity) {
		Customer customer = null;
		try {
			customer = customerBusiness.update(id, entity);
		} catch (CustomerValidationException e) {
			return badRequest();
		}
		
		if (Objects.isNull(customer))
			return notFound(id);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Customer> delete(@PathVariable("id") String id) {
		Customer c = customerBusiness.delete(id);
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Customer>(c, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Customer> get(@PathVariable("id") String id) {
		Customer c = customerBusiness.findById(id);
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Customer>(c, Objects.nonNull(c) ? HttpStatus.OK : HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/campaigns", method = RequestMethod.GET)
	public ResponseEntity<RestResponsePage<Campaign>> getAvailableCampaigns(@RequestParam("teamId") String teamId, Pageable pageable) {
		RestResponsePage<Campaign> campaignsByTeam = campaignBusiness.getCampaignsByTeam(teamId, pageable);
		
		return new ResponseEntity<RestResponsePage<Campaign>>(campaignsByTeam, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{customerId}/subscribe/{campaignId}", method = RequestMethod.POST)
	public ResponseEntity<Customer> getAvailableCampaigns(@PathVariable("customerId") String customerId, @PathVariable String campaignId) {
		Customer customer = campaignBusiness.subscribeCustomerToCampaign(customerId, campaignId);
		
		if (Objects.isNull(customer))
			return notFound(customerId);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Customer> conflict() {
		JsonMessage message = new JsonMessage();
		message.setMessage("User can't be created. There's other user registered with this email address.");
		message.setStatus("WARNING");
		return new ResponseEntity(message, HttpStatus.CONFLICT);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Customer> notFound(String id) {
		JsonMessage message = new JsonMessage();
		message.setMessage("Could not find Customer '" + id + "'.");
		message.setStatus("WARNING");
		
		return new ResponseEntity(message, HttpStatus.NOT_FOUND);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Customer> badRequest() {
		JsonMessage message = new JsonMessage();
		message.setMessage("Could not create or update Campaign. One or more fields are invalid.");
		message.setStatus("ERROR");
		
		return new ResponseEntity(message, HttpStatus.BAD_REQUEST);
	}
}
