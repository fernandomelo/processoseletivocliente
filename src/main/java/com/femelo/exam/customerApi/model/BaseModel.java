package com.femelo.exam.customerApi.model;

import java.time.format.DateTimeFormatter;

import org.springframework.data.annotation.Id;

public class BaseModel {
	public static DateTimeFormatter DATE_STRING_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Id
	private String id;
	private String createdDate;
	private String modifiedDate;
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
