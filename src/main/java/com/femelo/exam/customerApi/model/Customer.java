package com.femelo.exam.customerApi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Customer extends BaseModel {
	
	private String fullName;
	private String email;
	private String birthdate;
	private String teamId;
	private List<String> campaigns;
	
	public Customer() {
		this.campaigns = new ArrayList<String>();
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (Objects.nonNull(email))
			this.email = email.toLowerCase();
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public List<String> getCampaigns() {
		return this.campaigns;
	}
	
	public void addCampaign(String campaignId) {
		if (!this.campaigns.contains(campaignId))
			this.campaigns.add(campaignId);
	}
	
}
