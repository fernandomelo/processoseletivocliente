package com.femelo.exam.customerApi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.femelo.exam.customerApi.model.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

	public Customer findOneByEmail(String email);
	
	public Customer findByEmail(String email);
	
}