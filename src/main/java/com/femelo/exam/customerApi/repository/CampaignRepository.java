package com.femelo.exam.customerApi.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.model.Campaign;

@Service
public class CampaignRepository {
	
    private final RestTemplate restTemplate;
    
    public CampaignRepository(RestTemplateBuilder restTemplateBuilder
    		, @Value("${femelo.exam.campaignApi.host}") String campaignApiHost
    		, @Value("${femelo.exam.campaignApi.port}") int campaignApiPort) {
        this.restTemplate = restTemplateBuilder.rootUri("http://" + campaignApiHost + ":" + campaignApiPort).build();
    }
    
    @SuppressWarnings("unchecked")
	public RestResponsePage<Campaign> getAllCampaigns(Pageable pageable) {
    	Map<String, String> params = new HashMap<String, String>();
    	
    	if (Objects.nonNull(pageable)) {
    		params.put("page", Integer.toString(pageable.getPageNumber()));
    		params.put("size", Integer.toString(pageable.getPageSize()));
    	}
    	
        return this.restTemplate.getForObject("/campaign/?page={page}&sort=modifiedDate,desc&sort=createdDate,desc&size={size}", RestResponsePage.class, params);
    }
    
    @SuppressWarnings("unchecked")
	public RestResponsePage<Campaign> getModifiedCampaigns(String reference, Pageable pageable) {
    	Map<String, String> params = new HashMap<String, String>();
    	
    	if (Objects.nonNull(pageable)) {
    		params.put("page", Integer.toString(pageable.getPageNumber()));
    		params.put("size", Integer.toString(pageable.getPageSize()));
    	}
    	
    	params.put("reference", reference);
    	
        return this.restTemplate.getForObject("/campaign/modified?reference={reference}&page={page}&sort=modifiedDate,desc&sort=createdDate,desc&size={size}", RestResponsePage.class, params);
    }
    
    @SuppressWarnings("unchecked")
	public RestResponsePage<Campaign> getCreatedCampaigns(String reference, Pageable pageable) {
    	Map<String, String> params = new HashMap<String, String>();
    	
    	if (Objects.nonNull(pageable)) {
    		params.put("page", Integer.toString(pageable.getPageNumber()));
    		params.put("size", Integer.toString(pageable.getPageSize()));
    		
    		if (Objects.nonNull(pageable.getSort()))
    			params.put("sort", pageable.getSort().toString());
    		else
    			params.put("sort", "");
    	}
    	
    	params.put("reference", reference);
    	
        return this.restTemplate.getForObject("/campaign/created?reference={reference}&page={page}&sort=modifiedDate,desc&sort=createdDate,desc&size={size}", RestResponsePage.class, params);
    }

	public void linkCampaignAndCustomer(String campaignId, String customerId) {
		Map<String, String> params = new HashMap<String, String>();
		
		params.put("campaignId", campaignId);
		params.put("customerId", customerId);
		
		this.restTemplate.postForObject("/campaign/{campaignId}/link?customerId={customerId}", null, Campaign.class, params);
	}

}
