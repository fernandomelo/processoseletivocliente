package com.femelo.exam.customerApi.localRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.model.Campaign;

@Component
public class CampaignLocalRepository {

	private static String highestCreatedDate = "";
	private static String highestModifiedDate = "";

	public static String getHighestCreatedDate() {
		return highestCreatedDate;
	}

	public static String getHighestModifiedDate() {
		return highestModifiedDate;
	}

	private static List<Campaign> localCampaigns = new ArrayList<Campaign>();
	private static Queue<String> pendingSubscriptions = new ConcurrentLinkedDeque<>(); 

	private static final ObjectMapper mapper = new ObjectMapper();

	public RestResponsePage<Campaign> getCampaignsByTeam(String teamId, Pageable pageable) {
		RestResponsePage<Campaign> page = new RestResponsePage<>();

		int totalSize = (int) localCampaigns.stream().filter(x -> x.getTeamId().equals(teamId)).count();

		int lastPage = (int) Math.floor(totalSize / pageable.getPageSize());

		List<Campaign> items = localCampaigns.stream().filter(x -> x.getTeamId().equals(teamId))
				.skip(pageable.getPageNumber() * pageable.getPageSize()).limit(pageable.getPageSize())
				.collect(Collectors.toList());

		page.setContent(items);
		page.setFirst(pageable.getPageNumber() * pageable.getPageSize() == 0);
		page.setNumber(pageable.getPageNumber());
		page.setSize(pageable.getPageSize());
		page.setNumberOfElements(items.size());
		page.setSort(null);
		page.setTotalElements(totalSize);
		page.setTotalPages(lastPage + 1);
		page.setLast(pageable.getPageNumber() >= lastPage);

		return page;
	}
	
	public String popPendingSubscription() {
		return pendingSubscriptions.poll();
	}

	public void addCampaign(Campaign campaign) {
		if (campaign.getCreatedDate() != null && highestCreatedDate.compareTo(campaign.getCreatedDate()) < 0)
			highestCreatedDate = campaign.getCreatedDate();

		if (campaign.getModifiedDate() != null && highestModifiedDate.compareTo(campaign.getModifiedDate()) < 0)
			highestModifiedDate = campaign.getModifiedDate();

		if (!localCampaigns.stream().anyMatch(x -> x.getId().equals(campaign.getId()))) {
			localCampaigns.add(campaign);
		} else {
			Campaign existent = localCampaigns.stream().filter(x -> x.getId().equals(campaign.getId())).findFirst()
					.get();
			int index = localCampaigns.indexOf(existent);
			localCampaigns.set(index, campaign);
		}
	}

	public void addCampaigns(List<Campaign> list) {
		List<Campaign> newList = mapper.convertValue(list, new TypeReference<List<Campaign>>() {
		});

		for (Campaign current : newList) {
			addCampaign(current);
		}
	}

	public void queueSubscription(String customerId, String campaignId) {
		pendingSubscriptions.add(customerId + "-" + campaignId);
	}
}
