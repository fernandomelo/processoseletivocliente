package com.femelo.exam.customerApi.helper;

public class CustomerValidationException extends Exception {
	
	private static final long serialVersionUID = -6618712064483030624L;

	public CustomerValidationException(String message) {
		super(message);
	}

}
