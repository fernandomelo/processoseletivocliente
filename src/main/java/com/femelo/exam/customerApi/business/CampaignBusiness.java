package com.femelo.exam.customerApi.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.localRepository.CampaignLocalRepository;
import com.femelo.exam.customerApi.model.Campaign;
import com.femelo.exam.customerApi.model.Customer;
import com.femelo.exam.customerApi.repository.CustomerRepository;

@Component
public class CampaignBusiness {
	
	@Autowired
	private CampaignLocalRepository campaignRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public CampaignBusiness(CampaignLocalRepository campaignLocalRepository, CustomerRepository customerRepository) {
		this.campaignRepository = campaignLocalRepository;
		this.customerRepository = customerRepository;
	}
	
	public RestResponsePage<Campaign> getCampaignsByTeam(String teamId, Pageable pageable) { 
		return campaignRepository.getCampaignsByTeam(teamId, pageable);
	}

	public Customer subscribeCustomerToCampaign(String customerId, String campaignId) {
		Customer customer = customerRepository.findOne(customerId);
		
		if (customer == null)
			return null;
		
		customer.addCampaign(campaignId);
		customerRepository.save(customer);
		
		campaignRepository.queueSubscription(customerId, campaignId);
		
		return customer;
	}

}
