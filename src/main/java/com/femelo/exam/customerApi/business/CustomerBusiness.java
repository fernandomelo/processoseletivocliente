package com.femelo.exam.customerApi.business;

import java.time.Instant;
import java.util.Objects;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.femelo.exam.customerApi.helper.CustomerValidationException;
import com.femelo.exam.customerApi.model.Customer;
import com.femelo.exam.customerApi.repository.CustomerRepository;

@Component
public class CustomerBusiness {

	@Autowired
	private CustomerRepository customerRepository;
	
	public CustomerBusiness(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public Customer create(Customer entity) throws CustomerValidationException {
		entity.setCreatedDate(Instant.now().toString());
		entity.setId(null);
		entity.setModifiedDate(null);
		
		if (!validateModel(entity))
			throw new CustomerValidationException("Customer is invalid or incomplete.");

		Customer inserted = customerRepository.insert(entity);

		return inserted;
	}

	public Customer update(String id, Customer entity) throws CustomerValidationException {
		Customer c = customerRepository.findOne(id);

		if (Objects.isNull(c))
			return null;

		c.setId(id);
		c.setModifiedDate(Instant.now().toString());

		c.setFullName(entity.getFullName());
		c.setTeamId(entity.getTeamId());
		c.setBirthdate(entity.getBirthdate());
		
		if (!validateModel(entity))
			throw new CustomerValidationException("Customer is invalid or incomplete.");

		c = customerRepository.save(c);

		return c;
	}

	public Customer findByEmail(String email) {
		return customerRepository.findByEmail(email);
	}

	public Customer delete(String id) {
		Customer c = customerRepository.findOne(id);

		if (Objects.nonNull(c))
			customerRepository.delete(id);

		return c;
	}

	public Customer findById(String id) {
		return customerRepository.findOne(id);
	}

	public boolean validateModel(Customer entity) {
		if (Objects.isNull(entity)
				|| entity.getFullName() == null || entity.getFullName().isEmpty()
				|| entity.getBirthdate() == null || entity.getBirthdate().isEmpty()
				|| entity.getEmail() == null || entity.getEmail().isEmpty() || !isValidEmailAddress(entity.getEmail())
				|| entity.getTeamId() == null || entity.getTeamId().isEmpty())				
			return false;
		
		return true;
	}

	public static boolean isValidEmailAddress(String email) {
		String regex = "^(.+)@(.+)$";
		return Pattern.matches(regex, email);
	}
}
