package com.femelo.exam.customerApi;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.localRepository.CampaignLocalRepository;
import com.femelo.exam.customerApi.model.Campaign;
import com.femelo.exam.customerApi.repository.CampaignRepository;

@Component
@Profile({"mlab", "local"})
public class CustomerApiJob {
	
	@Autowired
	private CampaignLocalRepository campaignLocalRepository;
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	public CustomerApiJob(CampaignLocalRepository campaignLocalRepository, CampaignRepository campaignRepository) {
		this.campaignLocalRepository = campaignLocalRepository;
		this.campaignRepository = campaignRepository;
	}
	
    @Scheduled(fixedDelay = 5000)
    public void sendCustomerApplicationsToCampaigns() {
    	for (String subscription = campaignLocalRepository.popPendingSubscription(); subscription != null; subscription = campaignLocalRepository.popPendingSubscription()) {
			String customerId = subscription.split("-")[0];
			String campaignId = subscription.split("-")[1];
			
			try {
				campaignRepository.linkCampaignAndCustomer(campaignId, customerId);
			} catch (Exception e) {
				campaignLocalRepository.queueSubscription(customerId, campaignId);
			}
    	}
    }
    
    @Scheduled(fixedDelay = 5000)
    public void checkIfExistsNewCampaigns() {
    	int page = 0;
    	int size = 1;
    	boolean last = false;
    	
    	do {
    		RestResponsePage<Campaign> pageResponse = campaignRepository.getCreatedCampaigns(CampaignLocalRepository.getHighestCreatedDate(), new PageRequest(page, size));
    		
    		campaignLocalRepository.addCampaigns(pageResponse.getContent());
    		
    		if (pageResponse.isLast())
    			last = true;
    		
    		page++;
    	} while (!last);
    }
    
    @Scheduled(fixedDelay = 5000)
    public void checkIfExistsModifiedCampaigns() {
    	int page = 0;
    	int size = 1;
    	boolean last = false;
    	
    	do {
    		RestResponsePage<Campaign> pageResponse = campaignRepository.getModifiedCampaigns(CampaignLocalRepository.getHighestModifiedDate(), new PageRequest(page, size));
    		
    		campaignLocalRepository.addCampaigns(pageResponse.getContent());
    		
    		if (pageResponse.isLast())
    			last = true;
    		
    		page++;
    	} while (!last);
    }
    
    @PostConstruct
    public void localStartUpLoad() {
    	int page = 0;
    	int size = 1;
    	boolean last = false;
    	
    	do {
    		RestResponsePage<Campaign> pageResponse = campaignRepository.getAllCampaigns(new PageRequest(page, size));
    		
    		campaignLocalRepository.addCampaigns(pageResponse.getContent());
    		
    		if (pageResponse.isLast())
    			last = true;
    		
    		page++;
    	} while (!last);
    }
}
