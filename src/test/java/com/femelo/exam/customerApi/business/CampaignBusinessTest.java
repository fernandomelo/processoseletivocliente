package com.femelo.exam.customerApi.business;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.femelo.exam.customerApi.helper.RestResponsePage;
import com.femelo.exam.customerApi.localRepository.CampaignLocalRepository;
import com.femelo.exam.customerApi.model.Campaign;
import com.femelo.exam.customerApi.model.Customer;
import com.femelo.exam.customerApi.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles(profiles = "test")
public class CampaignBusinessTest {

	CampaignBusiness campaignBusiness;
	
	@Mock
	private CampaignLocalRepository campaignRepository;
	
	@Mock
	private CustomerRepository customerRepository;
	
	private RestResponsePage<Campaign> fakeFullPage;
	private RestResponsePage<Campaign> fakeEmptyPage;
	
    @Before
    public void setUp() {
        campaignBusiness = new CampaignBusiness(campaignRepository, customerRepository);
        
        fakeFullPage = new RestResponsePage<Campaign>();
        fakeFullPage.setContent(Arrays.asList(new Campaign(), new Campaign()));
        
        fakeEmptyPage = new RestResponsePage<Campaign>();
        fakeEmptyPage.setContent(Arrays.asList());
    }
	
    @Test
	public void shouldSearchAndReturnCampaignsByTeam() {
		//when
		when(campaignRepository.getCampaignsByTeam("01", new PageRequest(0, 20)))
			.thenReturn(fakeFullPage);

		RestResponsePage<Campaign> page = campaignBusiness.getCampaignsByTeam("01", new PageRequest(0, 20));
		
		//then
		assertEquals(2, page.getContent().size());
		verify(campaignRepository, times(1)).getCampaignsByTeam(eq("01"), eq(new PageRequest(0, 20)));
	}
    
    @Test
	public void shouldSearchAndReturnEmptyIfNoneIfFound() {
		//when
		when(campaignRepository.getCampaignsByTeam("01", new PageRequest(0, 20)))
			.thenReturn(fakeEmptyPage);

		RestResponsePage<Campaign> page = campaignBusiness.getCampaignsByTeam("01", new PageRequest(0, 20));
		
		//then
		assertEquals(0, page.getContent().size());
		verify(campaignRepository, times(1)).getCampaignsByTeam(eq("01"), eq(new PageRequest(0, 20)));
	}
    
    @Test
    public void shouldReturnNullIfCustomerIsNotFound() {
    	//when
    	when(customerRepository.findOne("1234")).thenReturn(null);
    
    	//then
    	Customer subscribed = campaignBusiness.subscribeCustomerToCampaign("1234", "111");
    
    	assertNull(subscribed);
    	verify(customerRepository, times(1)).findOne(eq("1234"));
    	verify(customerRepository, times(0)).save(any(Customer.class));
    	verify(campaignRepository, times(0)).queueSubscription(eq("1234"), eq("111"));
    }
    
    @Test
    public void shouldQueueSubscriptionIfCustomerIsFound() {
    	//given
    	Customer customer = new Customer();
    	customer.setId("1234");
    	
    	//when
    	when(customerRepository.findOne("1234")).thenReturn(customer);
    
    	//then
    	Customer subscribed = campaignBusiness.subscribeCustomerToCampaign("1234", "111");
    
    	assertNotNull(subscribed);
    	assertEquals("1234", subscribed.getId());
    	verify(customerRepository, times(1)).findOne(eq("1234"));
    	verify(customerRepository, times(1)).save(any(Customer.class));
    	verify(campaignRepository, times(1)).queueSubscription(eq("1234"), eq("111"));
    }
}
