package com.femelo.exam.customerApi.business;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.femelo.exam.customerApi.helper.CustomerValidationException;
import com.femelo.exam.customerApi.model.Customer;
import com.femelo.exam.customerApi.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles(profiles = "test")
public class CustomerBusinessTest {

	@Mock
	private CustomerRepository customerRepository;
	
	private CustomerBusiness customerBusiness;
	
    @Before
    public void setUp() {
        customerBusiness = new CustomerBusiness(customerRepository);
    }
    
    @Test
    public void shouldCreateWithValidUser() {
    	Customer customer = new Customer();
    	customer.setFullName("Teste Silva");
    	customer.setBirthdate("1990-01-01");
    	customer.setEmail("email@email.com");
    	customer.setTeamId("SP01");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
			fail("User should have been created without errors");
		}
    	
    	assertNotNull(returnedCustomer);
    	verify(customerRepository, times(1)).insert(any(Customer.class));
    }
    
    @Test
    public void shouldThrowExceptionCreatingWithInvalidName() {
    	Customer customer = new Customer();
    	customer.setFullName("");
    	customer.setBirthdate("1990-01-01");
    	customer.setEmail("email@email.com");
    	customer.setTeamId("SP01");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
		}
    	
    	assertNull(returnedCustomer);
    	verify(customerRepository, times(0)).insert(any(Customer.class));
    }
    
    @Test
    public void shouldThrowExceptionCreatingWithInvalidBirthdate() {
    	Customer customer = new Customer();
    	customer.setFullName("Teste Silva");
    	customer.setBirthdate("");
    	customer.setEmail("email@email.com");
    	customer.setTeamId("SP01");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
		}
    	
    	assertNull(returnedCustomer);
    	verify(customerRepository, times(0)).insert(any(Customer.class));
    }
    
    @Test
    public void shouldThrowExceptionCreatingWithEmptyEmail() {
    	Customer customer = new Customer();
    	customer.setFullName("Teste Silva");
    	customer.setBirthdate("1990-01-01");
    	customer.setEmail("");
    	customer.setTeamId("SP01");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
		}
    	
    	assertNull(returnedCustomer);
    	verify(customerRepository, times(0)).insert(any(Customer.class));
    }
    
    @Test
    public void shouldThrowExceptionCreatingWithInvalidEmail() {
    	Customer customer = new Customer();
    	customer.setFullName("Teste Silva");
    	customer.setBirthdate("1990-01-01");
    	customer.setEmail("email");
    	customer.setTeamId("SP01");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
		}
    	
    	assertNull(returnedCustomer);
    	verify(customerRepository, times(0)).insert(any(Customer.class));
    }
    
    @Test
    public void shouldThrowExceptionCreatingWithTeamId() {
    	Customer customer = new Customer();
    	customer.setFullName("Teste Silva");
    	customer.setBirthdate("1990-01-01");
    	customer.setEmail("email@email.com");
    	customer.setTeamId("");
    	
    	Customer returnedCustomer = null;
    	
    	when(customerRepository.insert(customer)).thenReturn(new Customer());
    	
    	try {
    		returnedCustomer = customerBusiness.create(customer);
		} catch (CustomerValidationException e) {
			e.printStackTrace();
		}
    	
    	assertNull(returnedCustomer);
    	verify(customerRepository, times(0)).insert(any(Customer.class));
    }
}
